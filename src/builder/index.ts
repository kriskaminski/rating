/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

import {
    Forms,
    NodeBlock,
    REGEX_IS_URL,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    isNumberFinite,
    isString,
    npgettext,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { RatingMatchCondition } from "./conditions/match";
import { RatingCompareCondition } from "./conditions/compare";
import { TShapes } from "../runner/shapes";

/** Assets */
import ICON from "../../assets/icon.svg";
import SHAPE_STAR from "../../assets/shape-star.svg";
import SHAPE_HEART from "../../assets/shape-heart.svg";
import SHAPE_THUMBS_UP from "../../assets/shape-thumbs-up.svg";
import SHAPE_THUMBS_DOWN from "../../assets/shape-thumbs-down.svg";
import SHAPE_PERSON from "../../assets/shape-person.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    alias: "rating",
    version: PACKAGE_VERSION,
    get label() {
        return pgettext("block:rating", "Rating");
    },
    icon: ICON,
})
export class Rating extends NodeBlock {
    public ratingSlot!: Slots.Number;

    @definition
    @affects("#icon")
    shape?: TShapes;

    @definition
    steps?: number;

    @definition
    imageURL?: string;

    @definition
    imageWidth?: string;

    @definition
    imageAboveText?: boolean;

    get icon() {
        return this.shape ? this.shapeIcon : ICON;
    }

    get shapeIcon() {
        switch (this.shape) {
            case "hearts":
                return SHAPE_HEART;
            case "thumbs-up":
                return SHAPE_THUMBS_UP;
            case "thumbs-down":
                return SHAPE_THUMBS_DOWN;
            case "persons":
                return SHAPE_PERSON;
        }

        return SHAPE_STAR;
    }

    static getShapeLabel(shape: TShapes, count: number) {
        switch (shape) {
            case "hearts":
                return npgettext("block:rating", "1 heart", "%1 hearts", count);
            case "thumbs-up":
                return npgettext(
                    "block:rating",
                    "1 thumb up",
                    "%1 thumbs up",
                    count
                );
            case "thumbs-down":
                return npgettext(
                    "block:rating",
                    "1 thumb down",
                    "%1 thumbs down",
                    count
                );
            case "persons":
                return npgettext(
                    "block:rating",
                    "1 person",
                    "%1 persons",
                    count
                );
            case "stars":
                return npgettext("block:rating", "1 star", "%1 stars", count);
        }
    }

    @slots
    defineSlot(): void {
        this.ratingSlot = this.slots.static({
            type: Slots.Number,
            reference: "rating",
            label: pgettext("block:rating", "Rating"),
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name(true, true);
        this.editor.description();
        this.editor.option({
            name: pgettext("block:rating", "Image"),
            form: {
                title: pgettext("block:rating", "Image"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "imageURL", undefined)
                    )
                        .label(pgettext("block:rating", "Image source URL"))
                        .inputMode("url")
                        .placeholder("https://")
                        .autoValidate((ref: Forms.Text) =>
                            ref.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(ref.value) ||
                                  (ref.value.length > 23 &&
                                      ref.value.indexOf(
                                          "data:image/jpeg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/png;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/svg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/gif;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 1 &&
                                      ref.value.charAt(0) === "/")
                                ? "pass"
                                : "fail"
                        ),
                    new Forms.Text(
                        "singleline",
                        Forms.Checkbox.bind(this, "imageWidth", undefined)
                    )
                        .label(
                            pgettext("block:rating", "Image width (optional)")
                        )
                        .width(100)
                        .align("center"),
                    new Forms.Checkbox(
                        pgettext(
                            "block:rating",
                            "Display image on top of the paragraph"
                        ),
                        Forms.Checkbox.bind(this, "imageAboveText", undefined)
                    ),
                ],
            },
            activated: isString(this.imageURL),
        });
        this.editor.explanation();

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:rating", "Shape"),
            form: {
                title: pgettext("block:rating", "Shape"),
                controls: [
                    new Forms.Radiobutton<TShapes>(
                        [
                            {
                                label: pgettext("block:rating", "Stars"),
                                value: "stars",
                            },
                            {
                                label: pgettext("block:rating", "Hearts"),
                                value: "hearts",
                            },
                            {
                                label: pgettext("block:rating", "Thumbs up"),
                                value: "thumbs-up",
                            },
                            {
                                label: pgettext("block:rating", "Thumbs down"),
                                value: "thumbs-down",
                            },
                            {
                                label: pgettext("block:rating", "Persons"),
                                value: "persons",
                            },
                        ],
                        Forms.Radiobutton.bind(this, "shape", undefined)
                    ).label(
                        pgettext(
                            "block:rating",
                            "Select the shape you want to use for your rating."
                        )
                    ),
                ],
            },
            activated: isString(this.shape),
        });

        this.editor.option({
            name: pgettext("block:rating", "Steps"),
            form: {
                title: pgettext("block:rating", "Number of steps"),
                controls: [
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "steps", undefined, 5)
                    )
                        .min(1)
                        .max(10),
                ],
            },
            activated: isNumberFinite(this.steps),
        });

        this.editor.groups.options();
        this.editor.required(this.ratingSlot);
        this.editor.visibility();
        this.editor.alias(this.ratingSlot);
        this.editor.exportable(this.ratingSlot);
    }

    @conditions
    defineConditions(): void {
        const match = this.conditions.group(
            pgettext("block:rating", "Match"),
            this.shapeIcon
        );
        const compare = this.conditions.group(
            pgettext("block:rating", "Compare"),
            this.shapeIcon
        );

        for (let stars = 1; stars <= (this.steps || 5); stars++) {
            match.template({
                condition: RatingMatchCondition,
                icon: this.shapeIcon,
                label: Rating.getShapeLabel(this.shape || "stars", stars),
                props: {
                    stars,
                },
            });
        }

        each(
            [
                {
                    mode: "equal" as "equal",
                    label: pgettext("block:rating", "Answer is equal to"),
                },
                {
                    mode: "below" as "below",
                    label: pgettext("block:rating", "Answer is lower than"),
                },
                {
                    mode: "above" as "above",
                    label: pgettext("block:rating", "Answer is higher than"),
                },
                {
                    mode: "between" as "between",
                    label: pgettext("block:rating", "Answer is between"),
                },
                {
                    mode: "defined" as "defined",
                    label: pgettext("block:rating", "Answer is specified"),
                },
                {
                    mode: "undefined" as "undefined",
                    label: pgettext("block:rating", "Answer is not specified"),
                },
            ],
            (condition) => {
                compare.template({
                    condition: RatingCompareCondition,
                    icon: this.shapeIcon,
                    label: condition.label,
                    props: {
                        slot: this.ratingSlot,
                        mode: condition.mode,
                    },
                });
            }
        );
    }
}
