/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    definition,
    editor,
    pgettext,
    tripetto,
} from "tripetto";
import { TMode } from "../../runner/conditions/mode";
import { Rating } from "..";

/** Assets */
import ICON from "../../../assets/icon.svg";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:compare`,
    version: PACKAGE_VERSION,
    context: Rating,
    icon: ICON,
    get label() {
        return pgettext("block:rating", "Compare rating");
    },
})
export class RatingCompareCondition extends ConditionBlock {
    @definition
    @affects("#name")
    mode: TMode = "equal";

    @definition
    @affects("#name")
    value?: number;

    @definition
    @affects("#name")
    to?: number;

    get rating() {
        return (
            (this.node?.block instanceof Rating && this.node.block) || undefined
        );
    }

    get icon() {
        return this.rating?.shapeIcon || ICON;
    }

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        if (this.slot) {
            switch (this.mode) {
                case "between":
                    return `\`${this.slot.toString(this.value)}\` ≤ @${
                        this.slot.id
                    } ≤ \`${this.slot.toString(this.to)}\``;
                case "defined":
                    return `@${this.slot.id} ${pgettext(
                        "block:rating",
                        "specified"
                    )}`;
                case "undefined":
                    return `@${this.slot.id} ${pgettext(
                        "block:rating",
                        "not specified"
                    )}`;
                case "above":
                case "below":
                case "equal":
                    return `@${this.slot.id} ${
                        this.mode === "above"
                            ? ">"
                            : this.mode === "below"
                            ? "<"
                            : "="
                    } ${`\`${this.slot.toString(this.value)}\``}`;
            }
        }

        return this.type.label;
    }

    @editor
    defineEditor(): void {
        const valueTo = new Forms.Numeric(
            Forms.Numeric.bind(this, "to", undefined, 0)
        )
            .min(0)
            .max(this.rating?.steps || 5)
            .visible(this.mode === "between");

        const group = new Forms.Group([
            new Forms.Numeric(Forms.Numeric.bind(this, "value", undefined, 0))
                .min(0)
                .max(this.rating?.steps || 5)
                .autoFocus(),
            valueTo,
        ]).visible(this.mode !== "defined" && this.mode !== "undefined");

        this.editor.form({
            title: pgettext("block:rating", "When answer:"),
            controls: [
                new Forms.Radiobutton<TMode>(
                    [
                        {
                            label: pgettext("block:rating", "Is equal to"),
                            value: "equal",
                        },
                        {
                            label: pgettext("block:rating", "Is lower than"),
                            value: "below",
                        },
                        {
                            label: pgettext("block:rating", "Is higher than"),
                            value: "above",
                        },
                        {
                            label: pgettext("block:rating", "Is between"),
                            value: "between",
                        },
                        {
                            label: pgettext("block:rating", "Is specified"),
                            value: "defined",
                        },
                        {
                            label: pgettext("block:rating", "Is not specified"),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equal")
                ).on((mode: Forms.Radiobutton<TMode>) => {
                    group.visible(
                        mode.value !== "defined" && mode.value !== "undefined"
                    );
                    valueTo.visible(mode.value === "between");
                }),
                group,
            ],
        });
    }
}
