import { TShapes } from "./shapes";

export interface IRating {
    readonly shape?: TShapes;
    readonly steps?: number;
    readonly imageURL?: string;
    readonly imageWidth?: string;
    readonly imageAboveText?: boolean;
}
