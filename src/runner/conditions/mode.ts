export type TMode =
    | "equal"
    | "below"
    | "above"
    | "between"
    | "defined"
    | "undefined";
