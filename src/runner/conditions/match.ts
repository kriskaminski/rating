/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import * as Tripetto from "tripetto-runner-foundation";

@Tripetto.block({
    type: "condition",
    identifier: PACKAGE_NAME,
    alias: "rating",
})
export class RatingMatchCondition extends Tripetto.ConditionBlock<{
    stars: number;
}> {
    @Tripetto.condition
    stars(): boolean {
        const ratingSlot = this.valueOf<number>("rating");

        return (ratingSlot && ratingSlot.value === this.props.stars) || false;
    }
}
