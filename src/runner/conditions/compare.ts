/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    castToNumber,
    condition,
    tripetto,
} from "tripetto-runner-foundation";
import { TMode } from "./mode";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:compare`,
})
export class RatingCompareCondition extends ConditionBlock<{
    mode: TMode;
    value?: number;
    to?: number;
}> {
    @condition
    compare(): boolean {
        const ratingSlot = this.valueOf<number>();

        if (ratingSlot) {
            switch (this.props.mode) {
                case "equal":
                    return (
                        ratingSlot.hasValue &&
                        ratingSlot.value === this.props.value
                    );
                case "below":
                    return (
                        ratingSlot.hasValue &&
                        ratingSlot.value < castToNumber(this.props.value)
                    );
                case "above":
                    return (
                        ratingSlot.hasValue &&
                        ratingSlot.value > castToNumber(this.props.value)
                    );
                case "between":
                    return (
                        ratingSlot.hasValue &&
                        ratingSlot.value >= castToNumber(this.props.value) &&
                        ratingSlot.value <= castToNumber(this.props.to)
                    );
                case "defined":
                    return ratingSlot.hasValue;
                case "undefined":
                    return !ratingSlot.hasValue;
            }
        }

        return false;
    }
}
