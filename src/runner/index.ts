/** Dependencies */
import { NodeBlock, assert } from "tripetto-runner-foundation";
import { IRating } from "./interface";
import "./conditions/compare";
import "./conditions/match";

export { TShapes } from "./shapes";

export abstract class Rating extends NodeBlock<IRating> {
    readonly ratingSlot = assert(this.valueOf<number>("rating"));
    readonly required = this.ratingSlot.slot.required || false;
    readonly steps = this.props.steps || 5;
    readonly shape = this.props.shape || "stars";
}
