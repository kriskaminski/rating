export type TShapes =
    | "stars"
    | "hearts"
    | "thumbs-up"
    | "thumbs-down"
    | "persons";
